# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyBlog::Application.config.secret_key_base = 'f902c08c794a5262430fbe3800300db1a2504a15686dd275e10d9d78be74f64c24f4f7abae84b60c9fd6c99e7846291c30b2ce82129f91029e0d67ba1737512d'
